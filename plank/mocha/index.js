#!/usr/bin/env node
'use strict';
const os = require('os');
const path = require('path');
const fs = require('fs-extra');
const chalk = require('chalk');
const rimraf = require('rimraf');
const inquirer = require('inquirer');
const pkg = require('./package.json');

const join = path.join;
const resolve = path.resolve;
const version = pkg.version;

const green = chalk.bold.green;	// Green bold text
const yellow = chalk.bold.yellow;	// Yellow bold text

const plankBinPath = '/usr/bin/plank';	// Plank dock binary path

const dock = 'dock.theme';
const deafaultDockPath = resolve(__dirname, dock);

const homeDir = os.homedir();
const themeDir = '.local/share/plank/themes/Mocha';
const localThemeDir = join(homeDir, themeDir);
const localDockPath = join(localThemeDir, dock);

const helpMessage = `
  Usage
    $ mocha-theme [option]

    Options
      --help, -h        Display help message
      --install, -i     Install theme
      --uninstall, -u   Uninstall theme
      --reinstall, -r   Reinstall theme
      --version, -v     Display installed version

    Examples
      $ mocha-theme
      $ mocha-theme --install
      $ mocha-theme --uninstall
      $ mocha-theme --version
      $ mocha-theme --help
`;

function installTheme() {
	// Check whether the theme is already installed
	if (fs.existsSync(localDockPath)) {
		console.log(yellow('Mocha is already installed'));
	} else {
		// Install the theme
		try {
			fs.copySync(deafaultDockPath, localDockPath);
			console.log(green('✔ Mocha was installed successfully'));
		} catch (err) {
			console.error(err);
		}
	}
}

function uninstallTheme() {
	if (fs.existsSync(localThemeDir)) {
		// Uninstall the theme
		try {
			rimraf.sync(localThemeDir);
			console.log(green('✔ Mocha was uninstalled successfully'));
		} catch (err) {
			console.error(err);
		}
	} else {
		console.log(yellow('Mocha is already uninstalled'));
	}
}

function reinstallTheme() {
	// Reinstall the theme
	if (fs.existsSync(localThemeDir)) {
		// Clean-up
		try {
			rimraf.sync(localThemeDir);
		} catch (err) {
			console.error(err);
		}
	}
	// Install the theme
	try {
		fs.copySync(deafaultDockPath, localDockPath);
		console.log(green('✔ Mocha was reinstalled successfully'));
	} catch (err) {
		console.error(err);
	}
}

function displayVersion() {
	// Display installed version
	console.log(green('Mocha theme version ' + version));
}

function displayHelp() {
	// Display help message
	console.log(helpMessage);
}

function exitMenu() {
	// Exit the options menu
	process.exit(0);
}

const options = [{
	type: 'list',
	name: 'installation',
	message: 'Mocha theme actions - What would you like to do?',
	choices: ['Install', 'Uninstall', 'Reinstall', 'Version', 'Help', 'Exit']
}];

function setAction(action) {
	// Decide on user action
	switch (action) {
		case ('Install'):
			installTheme();
			break;

		case ('Uninstall'):
			uninstallTheme();
			break;

		case ('Reinstall'):
			reinstallTheme();
			break;

		case ('Version'):
			displayVersion();
			break;

		case ('Help'):
			displayHelp();
			break;

		case ('Exit'):
			exitMenu();
			break;

		default:
			break;
	}
}

function askForAction() {
	// Ask user for action
	inquirer.prompt(options).then(answers => {
		const action = answers;
		setAction(action.installation);
	});
}

const mochaTheme = flags => {
	if (fs.existsSync(plankBinPath)) {
		if (flags.install) {
			// Install theme on -i or --install flag
			installTheme();
		} else if (flags.uninstall) {
			// Unistall theme on -u or --uninstall flag
			uninstallTheme();
		} else if (flags.reinstall) {
			// Reistall theme on -r or --reinstall flag
			reinstallTheme();
		} else {
			// Ask for action in the case of no flags
			askForAction();
		}
	} else {
		console.log(yellow('Plank dock was not found on your system'));
	}
};

module.exports = {mochaTheme, helpMessage};
