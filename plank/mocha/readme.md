<h1 align="center">
Mocha
</h1>

<h4 align="center">
☕ Dark theme for Plank dock
</h4>

<div align="center">
<a href="https://github.com/klauscfhq/mocha">
   <img
   src="https://cdn.rawgit.com/klauscfhq/mocha/8b7a5d1d/media/header.png" alt="Mocha" width="100%">
</a>
</div>

[![Build Status](https://travis-ci.org/klauscfhq/mocha.svg?branch=master)](https://travis-ci.org/klauscfhq/mocha) [![Dependency Status](https://dependencyci.com/github/klauscfhq/mocha/badge)](https://dependencyci.com/github/klauscfhq/mocha) [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/klauscfhq/mocha) [![Latest version](https://badge.fury.io/gh/klauscfhq%2Fmocha.svg)](https://github.com/klauscfhq/mocha/releases) [![npm](https://img.shields.io/npm/dt/mocha-theme.svg)](https://github.com/klauscfhq/mocha)

## Contents

- [Install](#install)
- [Usage](#usage)
- [Credits](#credits)
- [Team](#team)
- [License](#license)

## Install

```bash
npm install --global mocha-theme
```

## Usage

```
$ mocha-theme --help

  ☕ Dark theme for Plank dock

  Usage
    $ mocha-theme [option]

  Options
    --help, -h        Display help message
    --install, -i     Install theme
    --uninstall, -u   Uninstall theme
    --reinstall, -r   Reinstall theme
    --version, -v     Display installed version

  Examples
    $ mocha-theme
    $ mocha-theme --install
    $ mocha-theme --uninstall
    $ mocha-theme --version
    $ mocha-theme --help
```

## Credits

- [Background photo](https://unsplash.com/photos/XCY6L6GR-I0) by [Mervyn Chan](https://unsplash.com/@mervynckw)

## Team

- Klaus Sinani [(@klauscfhq)](https://github.com/klauscfhq)

## License

[MIT](license.md) © [Klaus Sinani](https://github.com/klauscfhq)
