#!/usr/bin/env node
'use strict';
const meow = require('meow');
const updateNotifier = require('update-notifier');
const pkg = require('./package.json');
const index = require('./index');

const mochaTheme = index.mochaTheme;
const helpMessage = index.helpMessage;

const cli = meow(helpMessage, {
	alias: {
		i: 'install',
		u: 'uninstall',
		r: 'reinstall',
		h: 'help'
	}
});

updateNotifier({pkg}).notify();

mochaTheme(cli.flags);
