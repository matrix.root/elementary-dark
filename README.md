# Installing
Run `./install.sh` by **terminal**, install **Mocha** in **plank preferences** and enjoy!
# Screenshots
![screenshot from 2018-04-22 00-10-30](https://user-images.githubusercontent.com/34302242/39089421-73a4bd26-45cf-11e8-853c-a11ecb13064a.png)
![screenshot from 2018-04-22 01-58-00](https://user-images.githubusercontent.com/34302242/39089485-9abacd46-45d0-11e8-936c-8a782057eec8.png)
