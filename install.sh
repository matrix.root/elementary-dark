#!/bin/bash

iconsPath="$HOME/.local/share/icons"
plankPath="$HOME/.local/share/plank/themes"

establish() {
    cd $1
        eval path=\${${1}Path}
        echo $path
        rm -rf $path/$2
        cp -r $2 $path
        echo -e "\033[0;32m$1 $2 installed\033[0m"
    cd ..
    case $1 in
        icons) gsettings set org.gnome.desktop.interface icon-theme $2;;
        plank) plank --preferences;;
    esac
}

setBlack() {
    cd $HOME/.config
    rm -f gtk-3.0/settings.ini
    if [ ! -d "gtk-3.0" ]; then
        mkdir "gtk-3.0"
    fi
    echo -e "[Settings]\ngtk-application-prefer-dark-theme=1" > gtk-3.0/settings.ini
}

establish icons la-capitaine-icon-theme
establish plank mocha
setBlack
